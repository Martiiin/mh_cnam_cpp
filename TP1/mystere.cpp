#include <iostream>

void bienvenue ()
{
    //initialisation de la variable prenom
    std::string prenom;
    //initialisation de la variable nom
    std::string nom;
    std::cout << "Quel est votre prénom et nom (veuillez les séparer par un espace) ? : " << std::endl;
    //On récupère les entrées dans chacune des variables (avec un espace)
    std::cin >> prenom >> nom;
    //On met une majuscule sur la première lettre
    prenom[0] = std::toupper(prenom[0]);

    //On met tout le nom de famille en majuscule
    for(int i = 0; i < nom.length(); i++)
    {
        nom[i] = std::toupper(nom[i]);
    }
    std::cout << "Salut " + prenom + " " + nom << std::endl;
}

int generatedRandomNumber()
{
    std::cout << "Nombre aléatoire crée ! " << std::endl;
    return std::rand()%1001;
}

void ordinateur()
{
    int randomNumber = generatedRandomNumber();
    //Int de la borne supérieur de la recherche
    int valeurHaut = 1000;
    //Int de la borne inférieur de la recherche
    int valeurBas = 0;
    //Valeur de l'ordinateur actuelle
    int valueUser;
    //Boolean pour sortir de la boucle
    bool estNonValide = true;
    while(estNonValide)
    {
        //On lui dit de prendre le milieu entre les deux bornes
        valueUser = (valeurBas + valeurHaut) / 2;
        //Test de l'égalité
        if(valueUser == randomNumber)
        {
            std::cout << "Bravo !!!!!! Vous avez trouvé le bon nombre, c'était bien le : " << valueUser << std::endl;
            estNonValide = false;
        }
        //Test de l'infériorité du random
        else if(valueUser > randomNumber)
        {
            std::cout << valueUser << " : C'est plus petit ! " << std::endl;
            //La borne haute est la valeur trouvé par l'ordinateur
            valeurHaut = valueUser;
        }
        //Test de la supériorité du random
        else
        {
            std::cout << valueUser << " : C'est plus grand ! " << std::endl;
            //La borne inférieur est la valeur trouvé par l'ordinateur
            valeurBas = valueUser;
        }
    }
}

void jouer()
{
    //Création du nombre aléatoire
    int randomNumber = generatedRandomNumber();
    //Value entré par l'utilisateur
    int valueUser;
    //Compteur du nombre d'essai
    int compteur = 0;
    //Boolean pour sortir de la boucle
    bool estNonValide = true;
    std::cout << "Entrez une proposition pour trouver le bon nombre : " << std::endl;
    while(estNonValide)
    {
        std::cin >> valueUser;
        //+1 pour le nombre d'essai
        compteur++;
        //test de s'il a trouvé le bon nombre
        if(valueUser == randomNumber)
        {
            std::cout << "Bravo vous avez trouvé le bon nombre, c'était bien le : " << valueUser << std::endl;
            std::cout << "Vous avez réussi à le faire en " << compteur << " essais, c'est pas si mal pour une promière fois ! " << std::endl;
            estNonValide = false;
        }
        //test si la valeur entré est plus grande que le nombre aléatoire
        else if(valueUser > randomNumber)
        {
            std::cout << "C'est plus petit !!" << std::endl;
        }
        //test si la valeur entré est plus petite que le nombre aléatoire
        else
        {
            std::cout << "C'est plus grand !!" << std::endl;
        }
    }
}

int main()
{
    //Fonction de bienvenue
    bienvenue();
    //Fonction qui return un entier aléatoire entre 0 et 1000
    generatedRandomNumber();
    //Fonction pour trouver le nombre aléatoire
    jouer();
    //Fonction pour que l'ordinateur trouve le nombre aléatoire
    ordinateur();
}