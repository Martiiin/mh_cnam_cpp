#include <iostream>

void Tennis(int pointA, int pointB)
{
    if(pointA == pointB)
    {
        switch (pointA)
        {
        case 0:
            std::cout << "0A" << std::endl;
            break;
        case 1:
            std::cout << "15A" << std::endl;
            break;
        case 2:
            std::cout << "30A" << std::endl;
            break;
        
        default:
            std::cout << "40A" << std::endl;
            break;
        }
    }
    else if(pointA > pointB + 1)
    {
        std::cout << "Victoire Joueur A" << std::endl;
    }
    else if(pointB > pointA + 1)
    {
        std::cout << "Victoire Joueur B" << std::endl;
    }
    else if(pointA > pointB)
    {
        std::cout << "Avantage Joueur A" << std::endl;
    }
    else
    {
        std::cout << "Avantage Joueur B" << std::endl;
    }
}

int main()
{
    int pointJoueurA;
    std::cout << "Ecrivez le nombre de coup gagné du Joueur A : " << std::endl;
    std::cin >> pointJoueurA;

    int pointJoueurB;
    std::cout << "Ecrivez le nombre de coup gagné du Joueur B : " << std::endl;
    std::cin >> pointJoueurB;

    //Fonction qui calcul le nombre de point dans le jeu ou le vainqueur ou celui qui est en avantage
    Tennis(pointJoueurA, pointJoueurB);
}