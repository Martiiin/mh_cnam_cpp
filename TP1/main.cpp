#include <iostream>

void helloWorld()
{
    std::cout << "Hello World!" << std::endl;
}

int somme(int a, int b)
{
    return a + b;
}

void inverse(int* a, int* b)
{
    int c = *a;
    *a = *b;
    *b = c;
}

void twoSumPtr(int a, int b, int* value)
{
    *value = a + b;
}

void twoSumRef(int a, int b, int &value){
    value = a + b;
}

void random_tab(int* tab, int taille)
{
    for(int k = 0; k < taille; k++)
    {
        for(int j = taille-1; j > 0; j--)
        {
            if(tab[j-1] > tab[j])
            {
                inverse(&tab[j], &tab[j-1]);
            }
        }
    }

    for(int i = 0; i < taille; i++)
    {
        std::cout << tab[i] << std::endl;
    }
}

void random_tab_Decroissant(int* tab, int taille)
{
    for(int k = 0; k < taille; k++)
    {
        for(int j = taille-1; j > 0; j--)
        {
            if(tab[j-1] < tab[j])
            {
                inverse(&tab[j], &tab[j-1]);
            }
        }
    }

    for(int i = 0; i < taille; i++)
    {
        std::cout << tab[i] << std::endl;
    }
}

void afterTriDoInverse(int* tableau, int taille, int estCroissant)
{
    if(estCroissant == 1)
    {
        random_tab(tableau, taille);
    }
    for(int i = 0; i < taille / 2 - 1; i++)
    {
        inverse(&tableau[i], &tableau[taille - i - 1]);
    }

    for(int i = 0; i < taille; i++)
    {
        std::cout << tableau[i] << std::endl;
    }
}

void createRandomTab(int* tableau, int taille)
{
    for(int i = 0; i < taille; i++)
    {
        tableau[i] = std::rand()%100000;
    }
}

int main()
{
    helloWorld();

    std::cout << somme(4,5) << std::endl;

    int val = 4;
    int val2 = 10;
    inverse(&val,&val2);
    std::cout << val << std::endl;
    std::cout << val2 << std::endl;

    int value = 42;
    twoSumPtr(val, val2, &value);
    std::cout << value << std::endl;

    int secondValue = 37;
    twoSumRef(val, val2, secondValue);
    std::cout << secondValue << std::endl << std::endl;

    int taille;

    std::cout << "Mettez la taille de votre tableau : " << std::endl;
    std::cin >> taille;
    int tab[taille];

    createRandomTab(tab, taille);

    std::cout << "Random tab : " << std::endl;
    random_tab(tab, taille);

    std::cout << "Le tableau décroissant : " << std::endl;
    random_tab_Decroissant(tab, taille);

    std::cout << "La double inversion : " << std::endl;
    afterTriDoInverse(tab, taille, 1);

    return 0;
}