#include <iostream>
#include <math.h>

#include "Point.h"

/**
 * @brief Constructeur par défaut.
 */
Point::Point()
{
    x = 0;
    y = 0;
}

/**
 * @brief Constructeur de la structure Point
 * @param _x La valeur de la coordonnée en x
 * @param _y La valeur de la coordonnée en y
 */
Point::Point(double _x, double _y)
{
    x = _x;
    y = _y;
}

/**
 * @brief Fonction qui calcul la longueur entre deux points
 * @param a Le premier point
 * @param b Le deuxième point
 * @return La longueur entre les deux points
 */
double Point::DistanceEntreDeuxPoints(Point a, Point b)
{
    double xDistance = a.x - b.x;
    double yDistance = a.y - b.y;
    return sqrt(xDistance * xDistance + yDistance * yDistance);
}

/**
 * @brief Méthode qui affiche les informations sur un point.
 * @details Affiche les coordonnée d'un point.
 */
void Point::Afficher()
{
    std::cout << "Le point a pour coordonnées : en x : " << x << " et en y : " << y << std::endl;
}
