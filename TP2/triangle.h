#ifndef TRIANGLE_H
#define TRIANGLE_H

#include "Point.h"

class Triangle
{
    public:
        Triangle();
        Triangle(Point Acoin, Point Bcoin, Point Ccoin);
        Point GetAcoin();
        void SetAcoin(Point Acoin);
        Point GetBcoin();
        void SetBcoin(Point Bcoin);
        Point GetCcoin();
        void SetCcoin(Point Ccoin);
        double CalculBaseTriangle();
        double CalculHauteurTriangle();
        double CalculSurfaceTriangle();
        double LongueurEntreDeuxPoints(Point a, Point b);
        bool TriangleEstIsocele();
        bool TriangleEstRectangle();
        bool TriangleEstEquilateral();

        void Afficher();
    private:
        Point Acoin;
        Point Bcoin;
        Point Ccoin;
};

#endif // TRIANGLE_H
