#include <iostream>
#include <math.h>

#include "cercle.h"

Cercle::Cercle()
{
    centre = Point(0, 0);
}

Cercle::Cercle(Point _centre, int _diametre)
{
    centre = _centre;
    diametre = _diametre;
}

double Cercle::Perimetre()
{
    return diametre * M_PI;
}

double Cercle::Surface()
{
    return ((diametre / 2)*(diametre / 2)) * M_PI;
}

bool Cercle::PointEstSurLeCercle(Point point)
{
    if(Point::DistanceEntreDeuxPoints(point, centre) == diametre)
    {
        return true;
    }
    return false;
}

bool Cercle::PointEstDansLeCercle(Point point)
{
    if(Point::DistanceEntreDeuxPoints(point, centre) < diametre)
    {
        return true;
    }
    return false;
}

void Cercle::Afficher()
{
    std::cout << "Le point au centre du cercle a un x de : " << centre.x << " et un y : " << centre.y << ", il a pour diametre : " << diametre << std::endl;
}
