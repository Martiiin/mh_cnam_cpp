#include <iostream>
#include <math.h>

#include "triangle.h"

Triangle::Triangle()
{
    Acoin = Point(0, 0);
    Bcoin = Point(0, 0);
    Ccoin = Point(0, 0);
}

Triangle::Triangle(Point _Acoin, Point _Bcoin, Point _Ccoin)
{
    Acoin = _Acoin;
    Bcoin = _Bcoin;
    Ccoin = _Ccoin;
}

Point Triangle::GetAcoin()
{
    return Acoin;
}

void Triangle::SetAcoin(Point _Acoin)
{
    Acoin = _Acoin;
}

Point Triangle::GetBcoin()
{
    return Bcoin;
}

void Triangle::SetBcoin(Point _Bcoin)
{
    Bcoin = _Bcoin;
}

Point Triangle::GetCcoin()
{
    return Ccoin;
}

void Triangle::SetCcoin(Point _Ccoin)
{
    Ccoin = _Ccoin;
}

double Triangle::CalculBaseTriangle()
{
    double ABLongueur = this->LongueurEntreDeuxPoints(this->GetAcoin(), this->GetBcoin());
    double BCLongueur = this->LongueurEntreDeuxPoints(this->GetBcoin(), this->GetCcoin());
    double ACLongueur = this->LongueurEntreDeuxPoints(this->GetAcoin(), this->GetCcoin());
    if(ABLongueur > BCLongueur && ABLongueur > ACLongueur)
    {
        return ABLongueur;
    }
    else if(BCLongueur > ABLongueur && BCLongueur > ACLongueur)
    {
        return BCLongueur;
    }
    else
    {
        return ACLongueur;
    }
}

double Triangle::CalculHauteurTriangle()
{
    double baseTriangle = this->CalculBaseTriangle();
    double surfaceTriangle = this->CalculSurfaceTriangle();
    return (2*surfaceTriangle)/baseTriangle;
}

double Triangle::CalculSurfaceTriangle()
{
    double ABLongueur = this->LongueurEntreDeuxPoints(this->GetAcoin(), this->GetBcoin());
    double BCLongueur = this->LongueurEntreDeuxPoints(this->GetBcoin(), this->GetCcoin());
    double ACLongueur = this->LongueurEntreDeuxPoints(this->GetAcoin(), this->GetCcoin());
    double demiPerimetre = (ABLongueur + BCLongueur + ACLongueur) / 2;

    return sqrt(demiPerimetre * (demiPerimetre - ABLongueur) * (demiPerimetre - BCLongueur) * (demiPerimetre - ACLongueur));
}

double Triangle::LongueurEntreDeuxPoints(Point a, Point b)
{
    double xDistance = a.x - b.x;
    double yDistance = a.y - b.y;
    return sqrt(xDistance * xDistance + yDistance * yDistance);
}

bool Triangle::TriangleEstIsocele()
{
    double ABLongueur = this->LongueurEntreDeuxPoints(this->GetAcoin(), this->GetBcoin());
    double BCLongueur = this->LongueurEntreDeuxPoints(this->GetBcoin(), this->GetCcoin());
    double ACLongueur = this->LongueurEntreDeuxPoints(this->GetAcoin(), this->GetCcoin());
    if(ABLongueur == BCLongueur || ABLongueur == ACLongueur || BCLongueur == ACLongueur)
    {
        return true;
    }
    return false;
}

bool Triangle::TriangleEstRectangle()
{
    double ABLongueur = this->LongueurEntreDeuxPoints(this->GetAcoin(), this->GetBcoin());
    double BCLongueur = this->LongueurEntreDeuxPoints(this->GetBcoin(), this->GetCcoin());
    double ACLongueur = this->LongueurEntreDeuxPoints(this->GetAcoin(), this->GetCcoin());
    bool res = true;
    if(ABLongueur >= BCLongueur && ABLongueur >= ACLongueur)
    {
        res = (ABLongueur*ABLongueur == (ACLongueur*ACLongueur + BCLongueur*BCLongueur));
        return res;
    }
    else if(ACLongueur >= BCLongueur && ACLongueur >= ABLongueur)
    {
        res = (ACLongueur*ACLongueur == (ABLongueur*ABLongueur + BCLongueur*BCLongueur));
        return res;
    }
    else
    {
        res = (BCLongueur*BCLongueur == (ACLongueur*ACLongueur + ABLongueur*ABLongueur));
        return res;
    }
}

bool Triangle::TriangleEstEquilateral()
{
    double ABLongueur = this->LongueurEntreDeuxPoints(this->GetAcoin(), this->GetBcoin());
    double BCLongueur = this->LongueurEntreDeuxPoints(this->GetBcoin(), this->GetCcoin());
    double ACLongueur = this->LongueurEntreDeuxPoints(this->GetAcoin(), this->GetCcoin());
    bool res = true;
    if(ABLongueur == BCLongueur && ABLongueur == ACLongueur)
    {
        return res;
    }
    return false;
}

/**
 * @brief Méthode qui affiche les informations sur un point.
 * @details Affiche les coordonnée d'un point.
 */
void Triangle::Afficher()
{
    std::cout << "Le triangle a pour Point A (x : y) : " << Acoin.x << " : " << Acoin.y << " Pour point B (x : y) : " << Bcoin.x << " : " << Bcoin.y << " Pour point C (x : y) : " << Ccoin.x << " : " << Ccoin.y << std::endl;
}
