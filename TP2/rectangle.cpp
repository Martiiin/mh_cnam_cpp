#include <iostream>
#include <math.h>

#include "Point.h"
#include "rectangle.h"

/**
 * @brief Constructeur par défaut.
 */
Rectangle::Rectangle()
{
    longueur = 1;
    largeur = 1;
    coinSuperieurGauche = Point(0, 0);
}

Rectangle::Rectangle(int _longueur, int _largeur, Point point)
{
    longueur = _longueur;
    largeur = _largeur;
    coinSuperieurGauche = point;
}

int Rectangle::GetLongueur()
{
    return longueur;
}

void Rectangle::SetLongueur(int longueur)
{
    this->longueur = longueur;
}

int Rectangle::GetLargeur()
{
    return largeur;
}

void Rectangle::SetLargeur(int largeur)
{
    this->largeur = largeur;
}

Point Rectangle::GetCoinSuperieurGauche()
{
    return coinSuperieurGauche;
}

void Rectangle::SetCoinSuperieurGauche(double x, double y)
{
    this->coinSuperieurGauche = Point(x, y);
}

double Rectangle::Perimetre()
{
    return (largeur * 2 + longueur * 2);
}

double Rectangle::Surface()
{
    return (longueur*largeur);
}

bool Rectangle::APlusGrandPerimetre(Rectangle rect)
{
    if(rect.Perimetre() > this->Perimetre())
    {
        return true;
    }
    return false;
}

bool Rectangle::APlusGrandeSurface(Rectangle rect)
{
    if(rect.Surface() > this->Surface())
    {
        return true;
    }
    return false;
}

void Rectangle::Afficher()
{
    std::cout << "La longueur du rectangle est de : " << longueur << " et sa largeur de : " << largeur << ". Son point de départ est avec x : " << coinSuperieurGauche.x << " et y : " << coinSuperieurGauche.y << std::endl;
}
