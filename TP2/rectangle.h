#ifndef RECTANGLE_H
#define RECTANGLE_H
#include "Point.h"


class Rectangle
{
    public:
        Rectangle();
        Rectangle(int longueur, int largeur, Point point);
        int GetLongueur();
        void SetLongueur(int longueur);
        int GetLargeur();
        void SetLargeur(int largeur);
        Point GetCoinSuperieurGauche();
        void SetCoinSuperieurGauche(double x, double y);
        double Perimetre();
        double Surface();
        bool APlusGrandPerimetre(Rectangle rect);
        bool APlusGrandeSurface(Rectangle rect);
        void Afficher();

    private:
        Point coinSuperieurGauche;
        int longueur;
        int largeur;
};

#endif // RECTANGLE_H
