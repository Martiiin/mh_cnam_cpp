#ifndef POINT_H
#define POINT_H

/**
 * @brief Structure permettant de représenter des points.
 */
struct Point
{
    double x;
    double y;

    Point();
    Point(double x, double y);
    static double DistanceEntreDeuxPoints(Point a, Point b);

    void Afficher();
};

#endif // POINT_H
