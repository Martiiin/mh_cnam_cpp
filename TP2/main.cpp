#include "Point.h"
#include "rectangle.h"
#include "triangle.h"
#include "cercle.h"
#include <iostream>

using namespace std;
int main()
{
    Point origine = Point();

    Point A = Point(0, 2);

    Point B = Point(1, 0);

    Point C = Point(0, -1);

    Point D = Point(2, 0);

    Rectangle rec_a = Rectangle(10, 10, B);
    rec_a.Afficher();
    cout << "Perimetre du rectangle : " << rec_a.Perimetre() << endl;
    cout << "Surface du rectangle : " << rec_a.Surface() << endl;

    Rectangle rec_b = Rectangle(10, 20, A);
    cout << "Test du perimetre superieur : " << rec_b.APlusGrandPerimetre(rec_a) << "     ##Resultat attendu 0" << endl;
    cout << "Test de la surface superieure : " << rec_a.APlusGrandeSurface(rec_b) << "     ##Resultat attendu 1" << endl;

    Triangle triangle_Rec = Triangle(origine, A, D);
    //Probleme d'arrondi avec les doubles pour cette exemple
    cout << "Test du triangle rectangle : " << triangle_Rec.TriangleEstRectangle() << endl;

    Triangle triangle_Iso = Triangle(origine, C, B);
    cout << "Test du triangle isocele : " << triangle_Iso.TriangleEstIsocele() << endl;

    cout << "Surface du triangle : " << triangle_Iso.CalculSurfaceTriangle() << endl;
    cout << "Hauteur du triangle : " << triangle_Iso.CalculHauteurTriangle() << endl;

    Cercle cercle_a = Cercle(origine, 2);
    cout << "Perimetre du cercle : " << cercle_a.Perimetre() << endl;
    cout << "Surface du cercle : " << cercle_a.Surface() << endl;
    cout << "Le point est sur le cercle : " << cercle_a.PointEstSurLeCercle(D) << endl;
    cout << "Le point est dans le cercle : " << cercle_a.PointEstDansLeCercle(C) << endl;



}
