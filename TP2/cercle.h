#ifndef CERCLE_H
#define CERCLE_H

#include "Point.h"


class Cercle
{
public:
    Cercle();
    Cercle(Point centre, int diametre);
    Point GetCentre();
    void SetCentre(Point centre);
    double Perimetre();
    double Surface();
    bool PointEstSurLeCercle(Point point);
    bool PointEstDansLeCercle(Point point);
    void Afficher();

private:
    Point centre;
    int diametre;

};

#endif // CERCLE_H
