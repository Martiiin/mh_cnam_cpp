#include <vector>
#include <iostream>
#include "case.h"
#ifndef LIGNE_H
#define LIGNE_H


class Ligne
{
public:
    Ligne(Case firstCase, Case secondCase, Case thirdCase);
    Ligne(Case firstCase, Case secondCase, Case thirdCase, Case fourthCase);
    Ligne(Case firstCase, Case secondCase, Case thirdCase, Case fourthCase, Case fifthCase, Case sixCase, Case sevenCase);
    bool LigneEstComplete(int jeton);
    bool LignePuissanceEstComplete(int jeton);
    bool ColonneDiagonaleEstComplete(int jeton);

private:
    std::vector<Case> ligne;
};

#endif // LIGNE_H
