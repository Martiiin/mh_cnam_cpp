#include "case.h"

Case::Case()
{
    ligne = 0;
    colonne = 0;
    value = 0;
}

Case::Case(int _ligne, int _colonne)
{
    ligne = _ligne;
    colonne = _colonne;
    value = 0;
}

int Case::GetLigne()
{
    return ligne;
}

int Case::GetColonne()
{
    return colonne;
}

int Case::GetValue()
{
    return value;
}
