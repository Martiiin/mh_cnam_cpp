#include "jeu.h"
#include "joueur.h"
#include "grillemorpion.h"
#include "grillepuissance.h"
#include "case.h"
#include <iostream>
#include <limits>

using namespace std;

Jeu::Jeu()
{

}

void Jeu::Start()
{
    int lancerJeu;
    cout << "Entrez 1 pour lancer le morpion et 2 pour lancer le puissance 4 : ";
    cin >> lancerJeu;
    if(lancerJeu == 1)
    {
        int recommencer = 2;
        do{
            //string nameJ1;
            //cout << "Entrez le prénom du premier joueur : ";
            //cin >> nameJ1;

            //string nameJ2;
            //cout << "Entrez le prénom du deuxième joueur : ";
            //cin >> nameJ2;

            Joueur j1 = Joueur("nameJ1", 1);
            Joueur j2 = Joueur("nameJ2", 2);

            GrilleMorpion grillemorpion = GrilleMorpion();
            grillemorpion.AffichageGrille();

            bool estPasFini = true;
            bool firstJoueur = true;
            bool tableauRempli = false;
            int ligneJoueur;
            int colonneJoueur;
            while(estPasFini && !tableauRempli)
            {
                if(firstJoueur)
                {
                    cout << "C est au premier joueur de jouer ! " << endl;
                }
                else
                {
                    cout << "C est au deuxieme joueur de jouer ! " << endl;
                }


                do{
                    cout << "Entrez le numero de ligne ou vous voulez mettre votre jeton : " << endl;
                    if ( LireChoix(ligneJoueur, 0, 2))
                    {
                        cout << "Vous avez choisi : " << ligneJoueur << endl;
                    }
                    cout << "Maintenant le numero de colonne : " << endl;
                    if ( LireChoix(colonneJoueur, 0, 2))
                    {
                        cout << "Vous avez choisi : " << colonneJoueur << endl;
                    }

                    if(!grillemorpion.CaseVide(Case(ligneJoueur, colonneJoueur)))
                    {
                        cout << "La case n est pas vide, veuillez en choisir une nouvelle case : " << endl;
                    }
                }
                while(!grillemorpion.CaseVide(Case(ligneJoueur, colonneJoueur)));


                if(firstJoueur)
                {
                    grillemorpion.DeposerJeton(Case(ligneJoueur, colonneJoueur), j1.value);
                    if(grillemorpion.VictoireJoueur(j1.value))
                    {
                        estPasFini = false;
                    }
                    else
                    {
                        firstJoueur = false;
                    }
                }
                else
                {
                    grillemorpion.DeposerJeton(Case(ligneJoueur, colonneJoueur), j2.value);
                    if(grillemorpion.VictoireJoueur(j2.value))
                    {
                        estPasFini = false;
                    }
                    else
                    {
                        firstJoueur = true;
                    }
                }
                if(grillemorpion.TableauRempli())
                {
                    tableauRempli = true;
                }
                grillemorpion.AffichageGrille();
            }
            if(!estPasFini)
            {
                if(firstJoueur)
                {
                    cout << "Victoire de " << j1.nom << "!!! Felicitation" << endl;
                }
                else
                {
                    cout << "Victoire de " << j2.nom << "!!! Felicitation" << endl;
                }
            }
            else
            {
                cout << "La grille est pleine, ce sera donc un match nul" << endl;
                cout << "Voulez-vous recommencer ? Si oui tapez 1, sinon tapez 2 pour sortir" << endl;
                if ( LireChoix(recommencer, 1, 2))
                {
                    cout << "Vous avez choisi : " << recommencer << endl;
                }
            }
        }while(recommencer == 1);
        cout << "Bonne continuation !";
    }
    else
    {
        Joueur j1 = Joueur("nameJ1", 1);
        Joueur j2 = Joueur("nameJ2", 2);
        int recommencer = 2;
        do{
            //string nameJ1;
            //cout << "Entrez le prénom du premier joueur : ";
            //cin >> nameJ1;

            //string nameJ2;
            //cout << "Entrez le prénom du deuxième joueur : ";
            //cin >> nameJ2;

            Joueur j1 = Joueur("nameJ1", 1);
            Joueur j2 = Joueur("nameJ2", 2);

            GrillePuissance grillepuissance = GrillePuissance();
            grillepuissance.AffichageGrille();

            bool estPasFini = true;
            bool firstJoueur = true;
            bool tableauRempli = false;
            int colonneJoueur;
            while(estPasFini && !tableauRempli)
            {
                if(firstJoueur)
                {
                    cout << "C est au premier joueur de jouer ! " << endl;
                }
                else
                {
                    cout << "C est au deuxieme joueur de jouer ! " << endl;
                }


                do{
                    cout << "Dans quel colonne voulez-vous poser le jeton ? : " << endl;
                    if ( LireChoix(colonneJoueur, 0, 6))
                    {
                        cout << "Vous avez choisi : " << colonneJoueur << endl;
                    }

                    if(!grillepuissance.CaseVide(colonneJoueur))
                    {
                        cout << "La case n est pas vide, veuillez en choisir une nouvelle case : " << endl;
                    }
                }
                while(!grillepuissance.CaseVide(colonneJoueur));


                if(firstJoueur)
                {
                    grillepuissance.DeposerJeton(colonneJoueur, j1.value);
                    if(grillepuissance.VictoireJoueur(j1.value))
                    {
                        estPasFini = false;
                    }
                    else
                    {
                        firstJoueur = false;
                    }
                }
                else
                {
                    grillepuissance.DeposerJeton(colonneJoueur, j2.value);
                    if(grillepuissance.VictoireJoueur(j2.value))
                    {
                        estPasFini = false;
                    }
                    else
                    {
                        firstJoueur = true;
                    }
                }
                if(grillepuissance.TableauRempli())
                {
                    tableauRempli = true;
                }
                grillepuissance.AffichageGrille();
            }
            if(!estPasFini)
            {
                if(firstJoueur)
                {
                    cout << "Victoire de " << j1.nom << "!!! Felicitation" << endl;
                }
                else
                {
                    cout << "Victoire de " << j2.nom << "!!! Felicitation" << endl;
                }
            }
            else
            {
                cout << "La grille est pleine, ce sera donc un match nul" << endl;
                cout << "Voulez-vous recommencer ? Si oui tapez 1, sinon tapez 2 pour sortir" << endl;
                if ( LireChoix(recommencer, 1, 2))
                {
                    cout << "Vous avez choisi : " << recommencer << endl;
                }
            }
        }while(recommencer == 1);
        cout << "Bonne continuation !";
    }
}

bool Jeu::LireChoix(int &value, int min, int max)
{
    cout << "Entrez un chiffre entre " << min << " et  " << max << " inclus : " ;
    while ( ! ( cin >> value ) || value < min || value > max )
    {
        if ( cin.fail() )
        {
            cout << "Saisie incorrecte, recommencez : ";
            cin.clear();
            cin.ignore( numeric_limits<streamsize>::max(), '\n' );
        }
        else
        {
            cout << "Le chiffre n'est pas entre " << min << " et  " << max << " inclus, recommencez : ";
        }
    }
    return true; // succès
}
