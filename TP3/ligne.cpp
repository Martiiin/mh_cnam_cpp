#include "ligne.h"
#include "case.h"

Ligne::Ligne(Case firstCase, Case secondCase, Case thirdCase)
{
    ligne = std::vector<Case> {firstCase, secondCase, thirdCase};
}

Ligne::Ligne(Case firstCase, Case secondCase, Case thirdCase, Case fourthCase)
{
    ligne = std::vector<Case> {firstCase, secondCase, thirdCase, fourthCase};
}

Ligne::Ligne(Case firstCase, Case secondCase, Case thirdCase, Case fourthCase, Case fifthCase, Case sixCase, Case sevenCase)
{
    ligne = std::vector<Case> {firstCase, secondCase, thirdCase, fourthCase, fifthCase, sixCase, sevenCase};
}

bool Ligne::LigneEstComplete(int jeton)
{
    for (Case item : ligne)
    {
        if(item.GetValue() != jeton)
        {
            return false;
        }
    }
    return true;
}

bool Ligne::LignePuissanceEstComplete(int jeton)
{
    int compteur = 0;
    for(Case item:ligne)
    {
        if(item.value != jeton)
        {
            compteur = 0;
        }
        else
        {
            compteur++;
            if(compteur > 3)
            {
                return true;
            }
        }
    }

    return false;
}
