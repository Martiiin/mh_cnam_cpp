#ifndef JOUEUR_H
#define JOUEUR_H

#include <string>

struct Joueur
{
    std::string nom;
    int value;

    Joueur(std::string nom, int value);
    int GetValue();
    std::string GetNom();
};

#endif // JOUEUR_H
