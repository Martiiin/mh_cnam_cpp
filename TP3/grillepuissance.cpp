#include "grillepuissance.h"

GrillePuissance::GrillePuissance()
{
    tab = std::vector<Case> {Case(0,0), Case(0,1), Case(0,2), Case(0,3),Case(0,4),Case(0,5),Case(0,6),
            Case(1,0), Case(1,1), Case(1,2),Case(1,3),Case(1,4),Case(1,5),Case(1,6),
            Case(2,0), Case(2,1), Case(2,2),Case(2,3),Case(2,4),Case(2,5),Case(2,6),
            Case(3,0), Case(3,1), Case(3,2),Case(3,3),Case(3,4),Case(3,5),Case(3,6),};
}

bool GrillePuissance::CaseVide(int colonne)
{
    for(int i = 0; i < 7; i++)
    {
        if(i == colonne)
        {
            if(tab[i].value == 0)
            {
                return true;
            }
        }
    }
    return false;
}

void GrillePuissance::DeposerJeton(int colonne,int jeton)
{
    bool estDejaEcrit = false;
    for(int i = (int)tab.size(); i >= 0; i--)
    {
        if(tab[i].colonne == colonne && !estDejaEcrit && tab[i].value == 0)
        {
            tab[i].value = jeton;
            estDejaEcrit = true;
        }
    }
}

bool GrillePuissance::LigneComplete(Ligne ligneEnter, int jeton)
{
    return ligneEnter.LignePuissanceEstComplete(jeton);
}

bool GrillePuissance::ColonneComplete(Ligne colonneEnter, int jeton)
{
    return colonneEnter.LigneEstComplete(jeton);
}

bool GrillePuissance::DiagonaleComplete(Ligne diagonaleEnter, int jeton)
{
    return diagonaleEnter.LigneEstComplete(jeton);
}

bool GrillePuissance::VictoireJoueur(int jeton)
{
    for(int i = 0; i < (int)tab.size(); i = i + 7)
    {
        if(LigneComplete(Ligne(tab[i], tab[i+1], tab[i+2], tab[i+3], tab[i+4], tab[i+5], tab[i+6]), jeton))
        {
            return true;
        }
    }
    for(int j = 0; j < (int)tab.size()/4; j++)
    {
        if(ColonneComplete(Ligne(tab[j], tab[j+7], tab[j+14], tab[j+21]), jeton))
        {
            return true;
        }
    }
    for(int k = 0; k < 4; k++)
    {
        if(DiagonaleComplete(Ligne(tab[k], tab[k+8], tab[k+16], tab[k+24]), jeton))
        {
            return true;
        }
    }
    for(int m = 3; m < 7; m++)
    {
        if(DiagonaleComplete(Ligne(tab[m], tab[m+6], tab[m+12], tab[m+18]), jeton))
        {
            return true;
        }
    }

    return false;
}

void GrillePuissance::AffichageGrille()
{
    std::cout << "=========Affichage de la grille===========" << std::endl;
    std::cout << "------------------------------------------" << std::endl;
    for(int i = 0; i < (int)tab.size(); i = i + 7)
    {
        std::cout << "| " << tab[i].GetValue() << " | " << tab[i+1].GetValue() << " | " <<  tab[i+2].GetValue() << " | " <<  tab[i+3].GetValue() << " | " <<  tab[i+4].GetValue() << " | " <<  tab[i+5].GetValue() << " | " <<  tab[i+6].GetValue() << " |" << std::endl;
    }
    std::cout << "------------------------------------------" << std::endl;
}

bool GrillePuissance::TableauRempli()
{
    for(int i = 0; i < 7; i++)
    {
        if(CaseVide(i))
        {
            return false;
        }
    }
    return true;
}
