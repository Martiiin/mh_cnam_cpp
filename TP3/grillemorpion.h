#include <vector>
#include <iostream>
#include "case.h"
#include "ligne.h"
#ifndef GRILLEMORPION_H
#define GRILLEMORPION_H


class GrilleMorpion
{
public:
    GrilleMorpion();
    std::vector<Case> GetTab();
    bool CaseVide(Case cell);
    void DeposerJeton(Case casePlay, int jeton);
    bool LigneComplete(Ligne ligneEnter, int jeton);
    bool ColonneComplete(Ligne colonneEnter, int jeton);
    bool DiagonaleComplete(Ligne diagonaleEnter, int jeton);
    bool VictoireJoueur(int jeton);
    void AffichageGrille();
    int IndexTab(int ligne, int colonne);
    bool TableauRempli();

private:
    std::vector<Case> tab;
};

#endif // GRILLEMORPION_H
