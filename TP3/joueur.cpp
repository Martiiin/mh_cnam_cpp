#include "joueur.h"

Joueur::Joueur(std::string _nom, int _value)
{
    nom = _nom;
    value = _value;
}

int Joueur::GetValue()
{
    return value;
}

std::string Joueur::GetNom()
{
    return nom;
}
