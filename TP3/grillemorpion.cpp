#include <vector>
#include <iostream>
#include "grillemorpion.h"
#include "case.h"
#include "ligne.h"

GrilleMorpion::GrilleMorpion()
{
    tab = std::vector<Case> {Case(0,0), Case(0,1), Case(0,2), Case(1,0), Case(1,1), Case(1,2), Case(2,0), Case(2,1), Case(2,2)};
}

std::vector<Case> GrilleMorpion::GetTab()
{
    return tab;
}

bool GrilleMorpion::CaseVide(Case cell)
{
    if(tab[IndexTab(cell.ligne, cell.colonne)].value == 0)
    {
        return true;
    }
    return false;
}

void GrilleMorpion::DeposerJeton(Case casePlay,int jeton)
{
    for(int i = 0; i < (int)tab.size(); i++)
    {
        if(tab[i].colonne == casePlay.colonne && tab[i].ligne == casePlay.ligne)
        {
            tab[i].value = jeton;
        }
    }
}

bool GrilleMorpion::LigneComplete(Ligne ligneEnter, int jeton)
{
    return ligneEnter.LigneEstComplete(jeton);
}

bool GrilleMorpion::ColonneComplete(Ligne colonneEnter, int jeton)
{
    return colonneEnter.LigneEstComplete(jeton);
}

bool GrilleMorpion::DiagonaleComplete(Ligne diagonaleEnter, int jeton)
{
    return diagonaleEnter.LigneEstComplete(jeton);
}

bool GrilleMorpion::VictoireJoueur(int jeton)
{
    for(int i = 0; i < (int)tab.size(); i = i + 3)
    {
        if(LigneComplete(Ligne(tab[i], tab[i+1], tab[i+2]), jeton))
        {
            return true;
        }
    }
    for(int j = 0; j < (int)tab.size()/3; j++)
    {
        if(ColonneComplete(Ligne(tab[j], tab[j+3], tab[j+6]), jeton))
        {
            return true;
        }
    }
    if(DiagonaleComplete(Ligne(tab[0], tab[4], tab[8]), jeton))
    {
        return true;
    }
    else if(DiagonaleComplete(Ligne(tab[2], tab[4], tab[6]), jeton))
    {
        return true;
    }

    return false;
}

void GrilleMorpion::AffichageGrille()
{
    std::cout << "=========Affichage de la grille===========" << std::endl;
    std::cout << "------------------------------------------" << std::endl;
    for(int i = 0; i < (int)tab.size(); i = i + 3)
    {
        std::cout << "| " << tab[i].GetValue() << " | " << tab[i+1].GetValue() << " | " <<  tab[i+2].GetValue() << " |" << std::endl;
    }
    std::cout << "------------------------------------------" << std::endl;
}

int GrilleMorpion::IndexTab(int ligne, int colonne)
{
    int index = 0;
    for(int i = 0; i < (int)tab.size(); i++)
    {
        if(ligne == tab[i].GetLigne() && colonne == tab[i].GetColonne())
        {
            index = i;
        }
    }
    return index;
}

bool GrilleMorpion::TableauRempli()
{
    for(int i = 0; i < (int)tab.size(); i++)
    {
        if(tab[i].value == 0)
        {
            return false;
        }
    }
    return true;
}
