#include <vector>
#include <iostream>
#include "case.h"
#include "ligne.h"
#ifndef GRILLEPUISSANCE_H
#define GRILLEPUISSANCE_H


class GrillePuissance
{
public:
    GrillePuissance();
    bool CaseVide(int colonne);
    void DeposerJeton(int colonne, int jeton);
    bool LigneComplete(Ligne ligneEnter, int jeton);
    bool ColonneComplete(Ligne colonneEnter, int jeton);
    bool DiagonaleComplete(Ligne diagonaleEnter, int jeton);
    bool VictoireJoueur(int jeton);
    void AffichageGrille();
    bool TableauRempli();
private:
    std::vector<Case> tab;
};

#endif // GRILLEPUISSANCE_H
