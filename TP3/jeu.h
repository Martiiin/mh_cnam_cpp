#ifndef JEU_H
#define JEU_H


class Jeu
{
public:
    Jeu();
    void Start();
    bool LireChoix(int &ligneJoueur, int min, int max);
};

#endif // JEU_H
