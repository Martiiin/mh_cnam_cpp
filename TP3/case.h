#ifndef CASE_H
#define CASE_H

struct Case
{
    int ligne;
    int colonne;
    int value;

    Case();
    Case(int ligne, int colonne);
    int GetLigne();
    int GetColonne();
    int GetValue();


};
#endif // CASE_H
